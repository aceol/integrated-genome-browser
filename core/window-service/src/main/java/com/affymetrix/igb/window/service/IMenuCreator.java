package com.affymetrix.igb.window.service;

import javax.swing.JMenuBar;

public interface IMenuCreator {
	public JMenuBar createMenu(String id);
}
