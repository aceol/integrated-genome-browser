package com.affymetrix.igb.shared;

/**
 * Contributed by Ido Tamir.
 */
public interface ViewPropertyNames {

    public static final String INITIAL_MAX_Y = "initialMaxY";
    public static final String INITIAL_MIN_Y = "initialMinY";
    public static final String INITIAL_COLOR = "initialColor";
    public static final String INITIAL_BACKGROUND = "initialBackground";
    public static final String INITIAL_GRAPH_STYLE = "initialGraphStyle";

}
