package com.affymetrix.igb.shared;

import java.awt.event.ActionEvent;

/**
 * For Panels that update the Track style defaults
 */
public class TrackPreferencesDefaults extends TrackPreferencesGUI {

    private static final long serialVersionUID = 1L;

    @Override
    protected void viewModeComboBoxActionPerformedA(ActionEvent evt) {
		// TODO Auto-generated method stub

    }

    @Override
    protected void floatCheckBoxActionPerformedA(ActionEvent evt) {
		// TODO Auto-generated method stub

    }

    @Override
    protected void YAxisCheckBoxActionPerformedA(ActionEvent evt) {
		// TODO Auto-generated method stub

    }

    @Override
    protected void graphStyleHeatMapComboBoxActionPerformedA(ActionEvent evt) {
		// TODO Auto-generated method stub

    }

    @Override
    protected void labelSizeComboBoxActionPerformedA(ActionEvent evt) {
		// TODO Auto-generated method stub

    }

    @Override
    protected void colorSchemeComboBoxActionPerformedA(ActionEvent evt) {
		// TODO Auto-generated method stub

    }

    @Override
    protected void labelFieldComboBoxActionPerformedA(ActionEvent evt) {
		// TODO Auto-generated method stub

    }

    @Override
    protected void strands2TracksCheckBoxActionPerformedA(ActionEvent evt) {
		// TODO Auto-generated method stub

    }

    @Override
    protected void strandsArrowCheckBoxActionPerformedA(ActionEvent evt) {
		// TODO Auto-generated method stub

    }

    @Override
    protected void strandsColorCheckBoxActionPerformedA(ActionEvent evt) {
		// TODO Auto-generated method stub

    }

    @Override
    protected void labelColorComboBoxActionPerformedA(ActionEvent evt) {
		// TODO Auto-generated method stub

    }

    @Override
    protected void strandsForwardColorComboBoxActionPerformedA(ActionEvent evt) {
		// TODO Auto-generated method stub

    }

    @Override
    protected void strandsReverseColorComboBoxActionPerformedA(ActionEvent evt) {
		// TODO Auto-generated method stub

    }

    @Override
    protected void buttonGroup1ActionPerformedA(ActionEvent evt) {
		// TODO Auto-generated method stub

    }

    @Override
    protected void backgroundColorComboBoxActionPerformedA(ActionEvent evt) {
		// TODO Auto-generated method stub

    }

    @Override
    protected void foregroundColorComboBoxActionPerformedA(ActionEvent evt) {
		// TODO Auto-generated method stub

    }

    @Override
    protected void stackDepthTextFieldActionPerformedA(ActionEvent evt) {
		// TODO Auto-generated method stub

    }

    @Override
    protected void trackNameTextFieldActionPerformedA(ActionEvent evt) {
		// TODO Auto-generated method stub

    }

    @Override
    protected void floatCheckBoxReset() {
		// TODO Auto-generated method stub

    }

    @Override
    protected void YAxisCheckBoxReset() {
		// TODO Auto-generated method stub

    }

    @Override
    protected void graphStyleHeatMapComboBoxReset() {
		// TODO Auto-generated method stub

    }

    @Override
    protected void labelSizeComboBoxReset() {
		// TODO Auto-generated method stub

    }

    @Override
    protected void colorSchemeComboBoxReset() {
		// TODO Auto-generated method stub

    }

    @Override
    protected void labelFieldComboBoxReset() {
		// TODO Auto-generated method stub

    }

    @Override
    protected void strands2TracksCheckBoxReset() {
		// TODO Auto-generated method stub

    }

    @Override
    protected void strandsArrowCheckBoxReset() {
		// TODO Auto-generated method stub

    }

    @Override
    protected void strandsColorCheckBoxReset() {
		// TODO Auto-generated method stub

    }

    @Override
    protected void labelColorComboBoxReset() {
		// TODO Auto-generated method stub

    }

    @Override
    protected void strandsForwardColorComboBoxReset() {
		// TODO Auto-generated method stub

    }

    @Override
    protected void strandsReverseColorComboBoxReset() {
		// TODO Auto-generated method stub

    }

    @Override
    protected void buttonGroup1Reset() {
		// TODO Auto-generated method stub

    }

    @Override
    protected void backgroundColorComboBoxReset() {
		// TODO Auto-generated method stub

    }

    @Override
    protected void foregroundColorComboBoxReset() {
		// TODO Auto-generated method stub

    }

    @Override
    protected void stackDepthTextFieldReset() {
		// TODO Auto-generated method stub

    }

    @Override
    protected void trackNameTextFieldReset() {
		// TODO Auto-generated method stub

    }

    @Override
    protected void stackDepthGoButtonActionPerformedA(ActionEvent evt) {
		// TODO Auto-generated method stub

    }

    @Override
    protected void stackDepthAllButtonActionPerformedA(ActionEvent evt) {
		// TODO Auto-generated method stub

    }

    @Override
    protected void selectAllButtonActionPerformedA(ActionEvent evt) {
		// TODO Auto-generated method stub

    }

    @Override
    protected void hideButtonActionPerformedA(ActionEvent evt) {
		// TODO Auto-generated method stub

    }

    @Override
    protected void clearButtonActionPerformedA(ActionEvent evt) {
		// TODO Auto-generated method stub

    }

    @Override
    protected void restoreToDefaultButtonActionPerformedA(ActionEvent evt) {
		// TODO Auto-generated method stub

    }

    @Override
    protected void stackDepthGoButtonReset() {
		// TODO Auto-generated method stub

    }

    @Override
    protected void stackDepthAllButtonReset() {
		// TODO Auto-generated method stub

    }

    @Override
    protected void selectAllButtonReset() {
		// TODO Auto-generated method stub

    }

    @Override
    protected void hideButtonReset() {
		// TODO Auto-generated method stub

    }

    @Override
    protected void clearButtonReset() {
		// TODO Auto-generated method stub

    }

    @Override
    protected void restoreToDefaultButtonReset() {
		// TODO Auto-generated method stub

    }

    @Override
    protected void labelCheckBoxActionPerformedA(ActionEvent evt) {
		// TODO Auto-generated method stub

    }

    @Override
    protected void labelCheckBoxReset() {
		// TODO Auto-generated method stub

    }

    @Override
    protected void strandsLabelReset() {
		// TODO Auto-generated method stub

    }
}
