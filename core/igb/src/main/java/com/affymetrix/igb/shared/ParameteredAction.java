package com.affymetrix.igb.shared;

/**
 *
 * @author hiralv
 */
public interface ParameteredAction {

    public void performAction(Object... parameters);
}
