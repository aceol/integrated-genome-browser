package com.affymetrix.genometry.quickload;

import com.affymetrix.genometry.AnnotatedSeqGroup;
import com.affymetrix.genometry.BioSeq;
import com.affymetrix.genometry.GenometryConstants;
import com.affymetrix.genometry.GenometryModel;
import com.affymetrix.genometry.SeqSpan;
import com.affymetrix.genometry.general.GenericFeature;
import com.affymetrix.genometry.general.GenericServer;
import com.affymetrix.genometry.general.GenericVersion;
import com.affymetrix.genometry.parsers.AnnotsXmlParser.AnnotMapElt;
import com.affymetrix.genometry.symloader.BNIB;
import com.affymetrix.genometry.symloader.SymLoader;
import com.affymetrix.genometry.symloader.TwoBitNew;
import com.affymetrix.genometry.symmetry.impl.SeqSymmetry;
import com.affymetrix.genometry.util.*;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.slf4j.LoggerFactory;

public class QuickloadServerType implements ServerTypeI {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(QuickloadServerType.class);

    enum QFORMAT {

        BNIB,
        VTWOBIT,
        TWOBIT,
        FA
    }

    private static final String name = "Quickload";
    public static final int ordinal = 20;
    private static final GenometryModel gmodel = GenometryModel.getInstance();
    private static final List<QuickLoadSymLoaderHook> quickLoadSymLoaderHooks = new ArrayList<>();
    /**
     * Private copy of the default Synonym lookup
     *
     * @see SynonymLookup#getDefaultLookup()
     */
    private static final SynonymLookup LOOKUP = SynonymLookup.getDefaultLookup();
    /**
     * For files too be looked up on server. *
     */
    private static final Set<String> quickloadFiles = new HashSet<>();

    /**
     * Add files to be looked up. *
     */
    static {
        quickloadFiles.add(Constants.ANNOTS_TXT);
        quickloadFiles.add(Constants.ANNOTS_XML);
        quickloadFiles.add(Constants.MOD_CHROM_INFO_TXT);
        quickloadFiles.add(Constants.LIFT_ALL_LFT);
        quickloadFiles.add(Constants.GENOME_TXT);
    }
    private static final QuickloadServerType instance = new QuickloadServerType();

    public static QuickloadServerType getInstance() {
        return instance;
    }

    private QuickloadServerType() {
        super();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int compareTo(ServerTypeI o) {
        return ordinal - o.getOrdinal();
    }

    @Override
    public int getOrdinal() {
        return ordinal;
    }

    @Override
    public String toString() {
        return name;
    }

    /**
     * @return true if file may not exist else false.
     */
    private boolean getFileAvailability(String fileName) {
        return fileName.equals(Constants.ANNOTS_TXT) || fileName.equals(Constants.ANNOTS_XML) || fileName.equals(Constants.LIFT_ALL_LFT);
    }

    /**
     * Gets files for a genome and copies it to it's directory.
     *
     * @param local_path	Local path from where mapping is to saved.
     */
    private boolean getAllFiles(GenericServer gServer, String genome_name, String local_path) {
        File file;
        Set<String> files = quickloadFiles;

        String server_path = gServer.URL + "/" + genome_name;
        local_path += "/" + genome_name;
        GeneralUtils.makeDir(local_path);
        boolean fileMayNotExist;
        for (String fileName : files) {
            fileMayNotExist = getFileAvailability(fileName);

            file = GeneralUtils.getFile(server_path + "/" + fileName, fileMayNotExist);

            if ((file == null && !fileMayNotExist)) {
                return false;
            }

            if (!GeneralUtils.moveFileTo(file, fileName, local_path)) {
                return false;
            }
        }

        return true;
    }

    @Override
    public boolean processServer(GenericServer gServer, String path) {
        File file = GeneralUtils.getFile(gServer.URL + Constants.CONTENTS_TXT, false);

        String quickloadStr = null;
        quickloadStr = (String) gServer.serverObj;

        QuickLoadServerModel quickloadServer = new QuickLoadServerModel(quickloadStr);

        List<String> genome_names = quickloadServer.getGenomeNames();
        if (!GeneralUtils.moveFileTo(file, Constants.CONTENTS_TXT, path)) {
            return false;
        }

        for (String genome_name : genome_names) {
            if (!getAllFiles(gServer, genome_name, path)) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Could not find all files for {0} !!!", gServer.serverName);
                return false;
            }
        }

        return true;
    }

    @Override
    public String formatURL(String url) {
        return url.endsWith("/") ? url : url + "/";
    }

    @Override
    public Object getServerInfo(String url, String name) {
        return formatURL(url);
    }

    @Override
    public String adjustURL(String url) {
        return url;
    }

    @Override
    public boolean loadStrategyVisibleOnly() {
        return false;
    }

    public static void addQuickLoadSymLoaderHook(QuickLoadSymLoaderHook quickLoadSymLoaderHook) {
        quickLoadSymLoaderHooks.add(quickLoadSymLoaderHook);
    }

    private QuickLoadSymLoader getQuickLoad(GenericVersion version, String featureName) {
        URI uri = determineURI(version, featureName);
        QuickLoadSymLoader quickLoadSymLoader = new QuickLoadSymLoader(uri, featureName, version.group);
        for (QuickLoadSymLoaderHook quickLoadSymLoaderHook : quickLoadSymLoaderHooks) {
            quickLoadSymLoader = quickLoadSymLoaderHook.processQuickLoadSymLoader(quickLoadSymLoader);
        }
        return quickLoadSymLoader;
    }

    private static URI determineURI(GenericVersion version, String featureName) {
        URI uri = null;

        if (version.gServer.URL == null || version.gServer.URL.length() == 0) {
            int httpIndex = featureName.toLowerCase().indexOf("http:");
            if (httpIndex > -1) {
                // Strip off initial characters up to and including http:
                // Sometimes this is necessary, as URLs can start with invalid "http:/"
                featureName = GeneralUtils.convertStreamNameToValidURLName(featureName);
                uri = URI.create(featureName);
            } else {
                uri = (new File(featureName)).toURI();
            }
        } else {
            String fileName = determineFileName(version, featureName);
            int httpIndex = fileName.toLowerCase().indexOf("http:");
            int httpsIndex = fileName.toLowerCase().indexOf("https:");
            int ftpIndex = fileName.toLowerCase().indexOf("ftp:");
            if (httpIndex > -1 || httpsIndex > -1 || ftpIndex > -1) {
                uri = URI.create(fileName);
            } else {
                uri = URI.create(
                        version.gServer.serverObj // Changed from 'version.gServer.URL' since quickload uses serverObj
                        + version.versionID + "/"
                        + determineFileName(version, featureName));
            }
        }

        return uri;
    }

    private static String determineFileName(GenericVersion version, String featureName) {
        URL quickloadURL;
        try {
            quickloadURL = new URL((String) version.gServer.serverObj);
        } catch (MalformedURLException ex) {
            ex.printStackTrace();
            return "";
        }

        QuickLoadServerModel quickloadServer = QuickLoadServerModel.getQLModelForURL(quickloadURL);
        List<AnnotMapElt> annotsList = quickloadServer.getAnnotsMap(version.versionID);

        // Linear search, but over a very small list.
        for (AnnotMapElt annotMapElt : annotsList) {
            if (annotMapElt.title.equals(featureName)) {
                return annotMapElt.fileName;
            }
        }
        return "";
    }

    @Override
    public void discoverFeatures(GenericVersion gVersion, boolean autoload) {
        // Discover feature names from QuickLoad
        try {
            URL quickloadURL = new URL((String) gVersion.gServer.serverObj);
            if (logger.isDebugEnabled()) {
                logger.debug("Discovering Quickload features for " + gVersion.versionName + ". URL:" + gVersion.gServer.serverObj);
            }

            QuickLoadServerModel quickloadServer = QuickLoadServerModel.getQLModelForURL(quickloadURL);
            List<String> typeNames = quickloadServer.getTypes(gVersion.versionName);
            if (typeNames == null) {
                String errorText = MessageFormat.format(GenometryConstants.BUNDLE.getString("quickloadGenomeError"), gVersion.gServer.serverName, gVersion.group.getOrganism(), gVersion.versionName);
                ErrorHandler.errorPanelWithReportBug(gVersion.gServer.serverName, errorText, Level.SEVERE);
                return;
            }

            for (String type_name : typeNames) {
                if (type_name == null || type_name.length() == 0) {
                    logger.warn("WARNING: Found empty feature name in " + gVersion.versionName + ", " + gVersion.gServer.serverName);
                    continue;
                }
                if (logger.isDebugEnabled()) {
                    logger.debug("Adding feature " + type_name);
                }
                Map<String, String> type_props = quickloadServer.getProps(gVersion.versionName, type_name);
                gVersion.addFeature(
                        new GenericFeature(
                                type_name, type_props, gVersion, getQuickLoad(gVersion, type_name), null, autoload));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void discoverChromosomes(Object versionSourceObj) {
    }

    @Override
    public boolean hasFriendlyURL() {
        return true;
    }

    @Override
    public boolean canHandleFeature() {
        return false;
    }

    /**
     * Discover genomes from Quickload
     *
     * @return false if there's an obvious failure.
     */
    @Override
    public boolean getSpeciesAndVersions(GenericServer gServer,
            GenericServer primaryServer, URL primaryURL,
            VersionDiscoverer versionDiscoverer) {
        URL quickloadURL = null;
        try {
            quickloadURL = new URL((String) gServer.serverObj);
        } catch (MalformedURLException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        QuickLoadServerModel quickloadServer = QuickLoadServerModel.getQLModelForURL(quickloadURL, primaryURL, primaryServer);

        if (quickloadServer == null) {
            logger.error("ERROR: No quickload server model found for server: " + gServer);
            return false;
        }

        quickloadServer.loadGenomeNames();
        List<String> genomeList = quickloadServer.getGenomeNames();
        if (genomeList == null || genomeList.isEmpty()) {
            logger.warn("WARNING: No species found in server: " + gServer);
            return false;
        }

        //update species.txt with information from the server.
        if (quickloadServer.hasSpeciesTxt()) {
            try {
                SpeciesLookup.load(quickloadServer.getSpeciesTxt());
            } catch (IOException ex) {
                logger.warn( "No species.txt found at this quickload server.", ex);
            }
        }
        for (String genomeID : genomeList) {
            String genomeName = LOOKUP.findMatchingSynonym(gmodel.getSeqGroupNames(), genomeID);
            String versionName, speciesName;
            // Retrieve group identity, since this has already been added in QuickLoadServerModel.
            Set<GenericVersion> gVersions = gmodel.addSeqGroup(genomeName).getEnabledVersions();
            if (!gVersions.isEmpty()) {
                // We've found a corresponding version object that was initialized earlier.
                versionName = GeneralUtils.getPreferredVersionName(gVersions);
//				speciesName = versionName2species.get(versionName);
                speciesName = versionDiscoverer.versionName2Species(versionName);
            } else {
                versionName = genomeName;
                speciesName = SpeciesLookup.getSpeciesName(genomeName);
            }
            versionDiscoverer.discoverVersion(genomeID, versionName, gServer, quickloadServer, speciesName);
        }
        return true;
    }

    //Note  exception may be thrown on invalid SSL certificates.
    public static boolean ping(URL url, int timeout) {
        try {
            if (url.getProtocol().equals("file")) {
                File f;
                try {
                    f = new File(url.toURI());
                } catch (URISyntaxException e) {
                    f = new File(url.getPath());
                }
                return f.exists();
            } else {
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setConnectTimeout(timeout);
                connection.setReadTimeout(timeout);
                connection.setRequestMethod("HEAD");
                int responseCode = connection.getResponseCode();
                return (200 <= responseCode && responseCode <= 399);
            }
        } catch (Exception exception) {
            return false;
        }
    }

    @Override
    public Map<String, List<? extends SeqSymmetry>> loadFeatures(SeqSpan span, GenericFeature feature) throws Exception {
        return (((QuickLoadSymLoader) feature.symL).loadFeatures(span, feature));
    }

    @Override
    public boolean isAuthOptional() {
        return false;
    }

    // Generate URI (e.g., "http://www.bioviz.org/das2/genome/A_thaliana_TAIR8/chr1.bnib")
    private String generateQuickLoadURI(String common_url, String vPath, QFORMAT Format) {
        Logger.getLogger(this.getClass().getName()).log(Level.FINE, "trying to load residues via Quickload");
        switch (Format) {
            case BNIB:
                common_url += "bnib";
                break;

            case FA:
                common_url += "fa";
                break;

            case VTWOBIT:
                common_url = vPath;
                break;

            case TWOBIT:
                common_url += "2bit";
                break;

        }

        return common_url;
    }

    private QFORMAT determineFormat(String common_url, String vPath) {

        for (QFORMAT format : QFORMAT.values()) {
            String url_path = generateQuickLoadURI(common_url, vPath, format);
            if (LocalUrlCacher.isValidURL(url_path)) {
                Logger.getLogger(this.getClass().getName()).log(Level.FINE,
                        "  Quickload location of " + format + " file: {0}", url_path);

                return format;
            }
        }

        return null;
    }

    private SymLoader determineLoader(String common_url, String vPath, AnnotatedSeqGroup seq_group, String seq_name) {
        QFORMAT format = determineFormat(common_url, vPath);

        if (format == null) {
            return null;
        }

        URI uri = null;
        try {
            uri = new URI(generateQuickLoadURI(common_url, vPath, format));
        } catch (URISyntaxException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }

        switch (format) {
            case BNIB:
                return new BNIB(uri, "", seq_group);

            case VTWOBIT:
                return new TwoBitNew(uri, "", seq_group);

            case TWOBIT:
                return new TwoBitNew(uri, "", seq_group);

//			case FA:
//				return new Fasta(uri, seq_group);
        }

        return null;
    }

    /**
     * Get the partial residues from the specified QuickLoad server.
     *
     * @return residue String.
     */
    private String GetQuickLoadResidues(
            GenericServer server, GenericVersion version, AnnotatedSeqGroup seq_group,
            String seq_name, String root_url, SeqSpan span, BioSeq aseq) {
        String common_url = "";
        String path = "";
        SymLoader symloader;
        try {
            URL quickloadURL = new URL((String) server.serverObj);
            QuickLoadServerModel quickloadServer = QuickLoadServerModel.getQLModelForURL(quickloadURL);
            path = quickloadServer.getPath(version.versionName, seq_name);
            common_url = root_url + path + ".";
            String vPath = root_url + quickloadServer.getPath(version.versionName, version.versionName) + ".2bit";

            symloader = determineLoader(common_url, vPath, seq_group, seq_name);

            if (symloader != null) {
                return symloader.getRegionResidues(span);
            }
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public boolean getResidues(GenericVersion version, String genomeVersionName,
            BioSeq aseq, int min, int max, SeqSpan span) {
        String seq_name = aseq.getID();
        AnnotatedSeqGroup seq_group = aseq.getSeqGroup();
        String residues = GetQuickLoadResidues(version.gServer, version, seq_group, seq_name, (String) version.gServer.serverObj, span, aseq);
        if (residues != null) {
            BioSeqUtils.addResiduesToComposition(aseq, residues, span);
            return true;
        }
        return false;
    }

    @Override
    public void removeServer(GenericServer server) {
        QuickLoadServerModel.removeQLModelForURL(server.URL);
    }

    @Override
    public boolean isSaveServersInPrefs() {
        return true;
    }

    @Override
    public String getFriendlyURL(GenericServer gServer) {
        if (gServer.serverObj == null) {
            return null;
        }
        String tempURL = (String) gServer.serverObj;
        if (tempURL.endsWith("/")) {
            tempURL = tempURL.substring(0, tempURL.length() - 1);
        }
        if (gServer.serverType != null) {
            tempURL = gServer.serverType.adjustURL(tempURL);
        }
        return tempURL;
    }

    @Override
    public boolean useMirrorSite(GenericServer gServer) {
        if (gServer.mirrorURL != null && gServer.mirrorURL != gServer.serverObj && LocalUrlCacher.isValidURL(gServer.mirrorURL)) {
            return true;
        }
        return false;
    }
}
