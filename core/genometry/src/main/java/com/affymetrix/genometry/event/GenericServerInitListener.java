package com.affymetrix.genometry.event;

public interface GenericServerInitListener {
	public void genericServerInit(GenericServerInitEvent evt);
}
