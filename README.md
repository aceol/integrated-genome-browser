# Integrated Genome Browser

The Integrated Genome Browser (IGB, pronounced ig-bee) is a fast, free, and flexible desktop genome browser. 

To get a copy, you can clone this repository or visit the [IGB Download](http://bioviz.org/igb/download.html) page at [BioViz.org](http://www.bioviz.org).

For documentation, visit:

* [BioViz Web site](http://www.bioviz.org)
* [Users Guide](https://wiki.transvar.org/display/igbman/Home) 
* [Developers Guide](https://wiki.transvar.org/display/igbdevelopers/Home)
* [IGB Channel on YouTube](https://www.youtube.com/channel/UC0DA2d3YdbQ55ljkRKHRBkg)

For information about development plans, visit the [IGB Issue Tracker site](https://jira.transvar.org/secure/RapidBoard.jspa?rapidView=7&view=planning&quickFilter=21&epics=visible).
